# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Ansible Repository for Jira server replication and upgrade ##

This repository list the steps to execute the Jira ansible playbooks.


###Pre-Requisites ###
* boto3
* python3
* pip3
* ansible 2.9 or higher
* AWS access Keys
* JIRA_HOME BACKUP (*.tar.gz) to be present on the control node home path.
* JIRA_SQL backup (sql.gz) to be present on the control node home path.
* * *

###Execution ###
**Step 1:**
Fill the below mentioned files which contain variables used by ansible.
```
group_vars/common.yml
group_vars/secrets.yml
```
**Step 2:**
Execute the playbooks
```
ansible-playbook main.yml
```

* * *
### JIRA Versioning ###
Jira tar versioning prior to version v7 was named as simple version name like 6.3.15 or 6.4.14 with just numbers.\
This has been changed in version v7 where the naming was changed to "software-x.x.x" with an additional text `software`\
added to the begining of the version number.

####JIRA UPGRADE VERSIONs TO FOLLOW ####
* clone 6.3.15
* upgrade 6.4.14
* upgrade version-7.0.11
* upgrade version-8.0.10

Ensure to disable the plugins that are enabled after every installation step untill the last upgrade version.
Set the below variables to control switch between upgrade mode and clone mode (below is for running clone)
```
clone: yes
legacy_jira_version: 6.3.15
```
After the first setup only 2 variables located in group_vars/common.yml will need changing(below is for running upgrade)
```
clone: no
upgrade_jira_version: 6.4.14 or software-7.0.11
```
* * *
#### Notes about UPM ####
UPM is the universal plugin manager used by the JIRA for managing plugins. The latest available upm jar that was stable \
was added as a link to the playbooks. Without this enabling disabling of plugins would not be possible via UI.

Download of the latest stable jar is a one time activity.
* * *
#### Notes about dbconfig.xml ####
This file(Currently located in JIRADATA folder) is used by jira for maintaining connection with the Jira Database, located on the same server.
JIRA server v8 has stopped support for Mysql's earlier versions. MySQL version 8.0 has many improvements which include a root password and internal property changes.
Thus the existing dbconfig.xml located in the existing jiradata is out of date. Because of this a provision for updating this file has been made for future purposes.

Simply name the file `dbconfig-{{upgrade_jira_version}}.xml` and put it in the templates folder. During upgrades this would be auto copied if found to be present.
* * *
#### Notes about Certificates ####
Certificates need to be placed in the files folder of the httpd role.

* * *
Note: Jira runs on port 8080 \
Default JIRA URL: ```https://<domainname>/```
### Contact ###
**Chester Dias**
